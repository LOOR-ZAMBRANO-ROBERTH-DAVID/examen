//Se importa la configuracion de la base de datos
//En el git lab se encuentra la coneccion a la base de datos
//y la configuracion del packagejson

const dbConnection = require('./connection')

//Cuerpo de la aplicacion
module.exports = app => {
    const connection = dbConnection

    //Proceso de registro
    app.post('/register', (req, res) => {
      
        const {
            cedula,
            apellido,
            nombres,
            direccion, 
            telefono
       
        }
        //Se realiza la inserccion en la base de datos 

        connection.query('INSERT INTO usuario SET ?', {
            cedula, apellido, nombres, direccion, telefono
        }, (err, result) => {
            //Una ver registrado hipoteticamente deberia direccionarnos al login
            res.redirect('/login')
        })
    })
}
