
//Coneccion a la base de datos

const mysql = require('mysql')

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'database'
})

connection.connect(function(err){
    /*Si existe algun error en la coneccion que se muestre lo sisguiente*/
    if(err) {
        console.log("Error al conectar con la base de datos")
    /*Si no existe algun error....*/

    } else {
        console.log("Conexión correcta a la base de datos")
    }
})

//Exportacion del modulo para usarlo en el registro
module.exports = connection